module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        derp: '#00a3e0'
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
