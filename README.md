This site was built using Squido's Docs template combined with Tailwind CSS.

1. To build the CSS, from the directory /source/content/stylesheets, run `npx tailwindcss -i ./style-input.css -o style-output.css`
2. To build the site, from the root directory, run `squido serve -b -w -c`
3. To run the Tailwind Config Viewer, from the directory /source/content/stylesheets, run `npx tailwind-config-viewer`
